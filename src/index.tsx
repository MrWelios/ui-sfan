
import { createBrowserHistory } from 'history';
import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.min.css';

import { configureStore } from './app/store';
import { App } from 'app/containers';

const store = configureStore();
export const history = createBrowserHistory();
render((
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App} />
        </Router>
    </Provider>
), document.getElementById('root'));
