import * as React from 'react';
import * as classNames from 'classnames';

import { Props, State } from './constants';
import * as style from './style.css';

export class SfanModal extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
    }

    close = () => {
        this.props.onClose(false);
    }

    render() {
        return (
            <React.Fragment>
                <div className={style['mfp-bg']}></div>
                <div className={style['mfp-wrap']}>
                    <div className={style['mfp-container']}>
                        <div className={style['mfp-content']}>
                            <div className={classNames(style['popup'], style['popup--md'], style['marx-auto'])}>
                                <button onClick={this.close} className={style['mfp-close']}>x</button>
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );

    }
}