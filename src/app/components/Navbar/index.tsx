import * as React from 'react';

import { Props, State } from './constants';

export class SfanNavbar extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
    }

    render() {


        return (
            <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Sfan admin control panel</a>
                <ul className="navbar-nav px-3">
                    <li className="nav-item text-nowrap">
                        {/* <a className="nav-link" href="#">Sign out</a> */}
                    </li>
                </ul>
            </nav>
        );

    }
}