export interface Props {
    to: string;
    className: string;
    isNotNewWindow?: boolean;
    onClick?: any;
}

export interface State {
}