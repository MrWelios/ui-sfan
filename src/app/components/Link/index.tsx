import * as React from 'react';
import { Link } from 'react-router-dom';

// import * as style from './style.css';
import { Props, State } from './constants';

export class SfanLink extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
    }

    isInternal(to) {
        if (to.indexOf("://") === -1) {
            return true;
        }
    }

    render() {

        const { to, children, isNotNewWindow, ...rest } = this.props;
        const isInternal = this.isInternal(to);

        if (isInternal) {
            return (<Link to={to} {...rest}>{children}</Link>);
        } else {
            return (<a href={to} target={isNotNewWindow ? '' : '_blank'} {...rest} rel="nofollow">{children}</a>);
        }

    }
}