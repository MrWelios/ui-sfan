export interface Props {
    value?: any;
    onSelect?(action: any): void;
}

export interface State {
    isOpen: boolean;
    startDate: Date;
    endDate: Date;
    selection: any;
}