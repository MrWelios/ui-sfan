import * as React from 'react';
import '!style-loader!css-loader!react-date-range/dist/styles.css';
import '!style-loader!css-loader!react-date-range/dist/theme/default.css';
import { DateRangePicker } from 'react-date-range';
import { Button, Row, Col, Input } from 'reactstrap';

import * as style from './style.css';
import { getDate } from 'app/services/appServices';
import { Props, State } from './constants';

export class SfanDateInterval extends React.Component<Props, State> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            isOpen: false,
            startDate: this.props.value ? this.props.value.startDate : null,
            endDate: this.props.value ? this.props.value.endDate : null,
            selection: {
                startDate: new Date(),
                endDate: new Date(),
            },
        };
    }

    dropDownClose = () => {
        (this.refs.dropDownItem as HTMLElement).style.display = 'none';
        this.setState({
            isOpen: false,
        });
    }

    handleClickOutside = (event) => {
        if (!event.path.includes(this.refs.dateIntervalContentRef)) {
            this.dropDownClose();
        }
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, false);
    }

    componentWillMount() {
        document.addEventListener('click', this.handleClickOutside, false);
    }

    handleSelect = (ranges) => {
        this.setState(ranges);
    }

    cancel = () => {
        this.setState({
            selection: {
                startDate: this.state.startDate ? this.state.startDate : new Date(),
                endDate: this.state.endDate ? this.state.endDate : new Date(),
            },
        });
        this.dropDownClose();
    }

    submit = () => {
        this.setState(this.state.selection);
        if (this.props.onSelect) {
            this.props.onSelect(this.state.selection);
        }
        this.dropDownClose();
    }

    openPicker = () => {
        const dropItem = this.refs.dropDownItem as HTMLElement;
        dropItem.style.display = this.state.isOpen ? 'none' : 'block';
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    render() {
        const selectionRange = {
            startDate: this.state.selection.startDate,
            endDate: this.state.selection.endDate,
            key: 'selection',
        };

        return (
            <React.Fragment>
                <div ref="dateIntervalContentRef">

                    <Row>
                        <Col md={6}>
                            <Input 
                                className={style['input']}
                                readOnly 
                                onClick={(_) => this.openPicker()} 
                                value={getDate(this.state.startDate)} 
                                type="string" 
                                name="datetime" 
                                id="date"
                                placeholder="from" />
                        </Col>
                        <Col md={6}>
                            <Input 
                                className={style['input']}
                                readOnly 
                                onClick={(_) => this.openPicker()} 
                                value={getDate(this.state.endDate)} 
                                type="string" 
                                name="datetime" 
                                id="date-to" 
                                placeholder="to" />
                        </Col>
                        <div className={style['drop-down']} ref="dropDownItem">
                            <Row>
                                <Col key="picker" md={12}>
                                    <DateRangePicker
                                        ranges={[selectionRange]}
                                        onChange={this.handleSelect}
                                        showSelectionPreview={true}
                                        moveRangeOnFirstSelection={false}
                                        months={2}
                                        direction="horizontal"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col key="action" md={12} className={style['action-button']}>
                                    <Button className={style['submit-button']} onClick={(_) => this.cancel()}>Cancel</Button>
                                    <Button className={style['submit-button']} 
                                        color="success" 
                                        onClick={(_) => this.submit()}
                                    >
                                        Submit
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Row>
                </div>
            </React.Fragment>
        );
    }
}
