export interface Props {
    // links: LinkInterface[];
}

export interface State {
}

export interface LinkInterface {
    label: string;
    url: string;
    child: any[];
}