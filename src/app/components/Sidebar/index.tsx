import * as React from 'react';
import * as classNames from 'classnames';
// import { Collapse } from 'reactstrap';

import * as style from './style.css';
import { Props, State } from './constants';
import { SfanLink } from 'app/components';

export class SfanSidebar extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
        this.state = {
        }
    }

    render() {
        return (
            <nav className={classNames('d-none', 'd-md-block', 'bg-light', style['sidebar'])}>
                <div className={style['sidebar-sticky']}>
                    <ul className="nav flex-column">
                        <li key={`/`} className="nav-item">
                            <SfanLink className={classNames(style['nav-link'])} to={`/`}>
                                Events
                            </SfanLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}