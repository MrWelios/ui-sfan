export interface Props {
    filterData: any;
    onSubmit?(action: any): void;
}

export interface State {
    filterValues: any;
}
