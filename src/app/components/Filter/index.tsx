import * as React from 'react';
import { Col, Button, Form, Label, Row, Input } from 'reactstrap';
import Select from 'react-select';


import * as style from './style.css';
import { Props, State } from './constants';
import { SfanDateInterval } from 'app/components';

export class SfanFilter extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);

        const initFilterValues = {};
        Object.keys(this.props.filterData).map((key) => {
            initFilterValues[key] = this.props.filterData[key].value;
        });

        this.state = {
            filterValues: initFilterValues,
        };
    }

    submitFilter = () => {
        if (this.props.onSubmit) {
            this.props.onSubmit(this.state.filterValues);
        }
    }

    onSelect = (value, key) => {
        const filterValues = this.state.filterValues;
        filterValues[key] = value;
        this.setState({
            filterValues,
        });
    }

    checkParent = (selectList, key) => selectList.filter((row) => {
        if (row.parentValue && this.props.filterData[key].parent 
            && (!this.state.filterValues[this.props.filterData[key].parent] 
                || !(
                    Array.isArray(this.state.filterValues[this.props.filterData[key].parent]) 
                    && this.state.filterValues[this.props.filterData[key].parent].map(data => data.value).includes(row.parentValue) 
                    || this.state.filterValues[this.props.filterData[key].parent].value === row.parentValue 
                    ) 
                )
        ) {
            return false;
        }
        return true;
    })

    isDisabled = (isDisable, key) => {
        if (!isDisable) {
            return false;
        }
        let disabled = false;
        const filterValues = this.state.filterValues;
        Object.keys(isDisable).forEach((disableKey) => {
            if (filterValues[disableKey].value === isDisable[disableKey].disabledValue) {
                disabled = true;
                // this.setState()
                if (filterValues[key].value !== isDisable[disableKey].value.value) {
                    filterValues[key] = isDisable[disableKey].value;
                    this.setState({filterValues});
                }
            }
        });
        return disabled;
    }

    renderFilterData = () => {
        const data = Object.keys(this.props.filterData).map((key) => {
            switch (this.props.filterData[key].type) {
                case ('select'): {
                    return (
                        <Col key={key} md={4}>
                            <Label for={key}>{this.props.filterData[key].label}</Label>
                            <Select
                                value={this.state.filterValues[key]}
                                options={this.checkParent(this.props.filterData[key].selectList, key)}
                                isDisabled={this.isDisabled(this.props.filterData[key].isDisable, key)}
                                isMulti={false}
                                onChange={(e) => this.onSelect(e, key)}
                            />
                        </Col>
                    );
                }
                case ('multi-select'): {
                    return (
                        <Col key={key} md={4}>
                            <Label for={key}>{this.props.filterData[key].label}</Label>
                            <Select
                                closeMenuOnSelect={false}
                                value={this.state.filterValues[key]}
                                options={this.checkParent(this.props.filterData[key].selectList, key)}
                                isMulti={true}
                                onChange={(e) => this.onSelect(e, key)}
                            />
                        </Col>
                    );
                }
                case ('time-interval'): {
                    return(
                        <Col key={key} md={4}>
                            <Label for={key}>{this.props.filterData[key].label}</Label>
                            <SfanDateInterval
                                value={this.props.filterData[key].value}
                                onSelect={(e) => this.onSelect(e, key)}
                            />
                        </Col>
                    );
                }
                case ('number'): {
                    return (
                        <Col key={key} md={4}>
                            <Label for={key}>{this.props.filterData[key].label}</Label>
                            <Input
                                type="number"
                                onChange={(e) => this.onSelect(e.target.value, key)}
                                value={this.state.filterValues[key]}
                            />
                        </Col>
                    );
                }
                default: 
                    return '';
            }
        });
        return (<Row form>{data}</Row>);
    }

    render() {
        return (
            <React.Fragment>
                <Form className={style['filter-form']}>
                    {this.renderFilterData()}
                    <Row>
                        <Col className={style['filter-action']} sm={{ size: 2, offset: 10 }}>
                            <Button className={style['submit-button']} onClick={(_) => this.submitFilter()}>Submit</Button>
                        </Col>
                    </Row>
                </Form>

            </React.Fragment>
        );
    }
}
