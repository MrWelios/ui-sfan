import * as React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

// import * as style from './style.css';
import { Props, State } from './constants';

export class SfanTablePagination extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);

        this.state = {
        }
    }

    paginationClick = (event) => {
        if (this.props.paginationClick) {
            this.props.paginationClick(event);
        }
    }

    renderNumbers() {
        let renderNumbers = [];
        for (let number = 1; number <= this.props.lastPage; number++) {
            if (this.props.lastPage <= 10) {
                renderNumbers.push(
                    <PaginationItem
                        key={'pagination' + number}
                        onClick={(_) => this.paginationClick(number)}
                        active={this.props.page === number}
                    >
                        <PaginationLink >
                            {number}
                        </PaginationLink>
                    </PaginationItem>
                )
            } else {
                switch (number) {
                    case 1:
                    case 2:
                    case 3:
                    case this.props.lastPage - 2:
                    case this.props.lastPage - 1:
                    case this.props.lastPage:
                    case this.props.page - 2:
                    case this.props.page - 1:
                    case this.props.page:
                    case this.props.page + 1:
                    case this.props.page + 2:
                        renderNumbers.push(
                            <PaginationItem
                                key={'pagination' + number}
                                onClick={(_) => this.paginationClick(number)}
                                active={this.props.page === number}
                            >
                                <PaginationLink >
                                    {number}
                                </PaginationLink>
                            </PaginationItem>
                        )
                        break;
                    case this.props.lastPage - 4:
                    case 4:
                        renderNumbers.push(
                            <PaginationItem disabled key={'pagination' + number}>
                                <PaginationLink >
                                    ...
                                </PaginationLink>
                            </PaginationItem>
                        )
                        break;
                    default:
                        break;
                }
            }

        }
        return renderNumbers;
    }

    render() {
        const { className } = this.props;
        return (
            <Pagination className={className}>
                <PaginationItem 
                    disabled={this.props.page === 1}
                    onClick={(_) => this.paginationClick(this.props.page - 1)}
                    >
                    <PaginationLink previous />
                </PaginationItem>
                {this.renderNumbers()}
                <PaginationItem 
                    disabled={this.props.page === this.props.lastPage}
                    onClick={(_) => this.paginationClick(this.props.page + 1)}
                    >
                    <PaginationLink next />
                </PaginationItem>
            </Pagination>
        );
    }
}