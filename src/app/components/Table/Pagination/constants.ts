export interface Props {
    className?: string;
    page: number;
    lastPage: number;
    paginationClick?: (action: any) => void;
}

export interface State {
}
