import * as React from 'react';
import { Table } from 'reactstrap';
// import * as classNames from 'classnames';

import * as style from './style.css';
import { Props, State } from './constants';
import { SfanTableHeaders } from './Header';
import { SfanTableBody } from './Body';
import { SfanTablePagination } from './Pagination';

export class SfanTable extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
    }

    handleOnClick = (event) => {
        if (this.props.onRowClick) {
            this.props.onRowClick(event);
        }
    }

    onMenuClick = (row, action) => {
        if (this.props.onMenuClick) {
            this.props.onMenuClick(row, action);
        }
    }

    renderCount = () => {
        if (this.props.count) {
            return (
                <p>
                    Showing {this.props.offset + 1} to {this.props.count > this.props.limit 
                            && this.props.offset + this.props.limit < this.props.count 
                            ? this.props.offset + this.props.limit 
                            : this.props.count} of {this.props.count} entries
                </p>
            );
        }
        return '';
    }

    paginationClick = (event) => {
        this.props.paginationClick((event - 1) * this.props.limit);
    }

    render() {
        return (
            <React.Fragment>
                <Table hover size="sm">
                    <SfanTableHeaders sort={this.props.sort} headers={this.props.headers} onSortChange={this.props.onSortChange} />
                    <SfanTableBody 
                        headers={this.props.headers} 
                        data={this.props.data} 
                        onRowClick={this.handleOnClick} 
                        onMenuClick={this.onMenuClick} 
                    />
                </Table>
                {this.renderCount()}
                <SfanTablePagination
                    className={style['table-pagination']}
                    page={(this.props.offset + this.props.limit) / this.props.limit}
                    lastPage={this.props.count ? Math.ceil(this.props.count / this.props.limit) : 0}
                    paginationClick={this.paginationClick}
                />
            </React.Fragment>
        );
    }
}