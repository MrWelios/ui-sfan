import { HeadersInterface, DataInterface } from '../constants';

export interface Props {
    headers: HeadersInterface;
    data: DataInterface[];
    onRowClick?(action: any): void;
    onMenuClick?(row: any, action: string): void;
}

export interface State {
    dropdownOpen: number;
}
