import * as React from 'react';
import * as classNames from 'classnames';
import { DropdownItem } from 'reactstrap';

import * as style from './style.css';
import { Props, State } from './constants';
import { SfanIcon } from 'app/components/Icon';

export class SfanTableBody extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);

        this.state = {
            dropdownOpen: null,
        };
    }

    handleOnClick = (row) => {
        if (this.props.onRowClick) {
            this.props.onRowClick(row);
        }
    }

    selectMenuAction = (row, action) => {
        if (this.props.onMenuClick) {
            this.props.onMenuClick(row, action);
        }
    }

    renderField = (row, index) => {
        const tableRow = Object.keys(this.props.headers).map((key) => {
            switch (this.props.headers[key].type) {
                case 'actions': {
                    return(
                        <td 
                            className={classNames({ [style['td-right']]: this.props.headers[key].isRight })} 
                            key={`cell-${index}-${key}`}
                        >
                            <div className={style['td-action']}>
                                {row[key].map((action) => {
                                    if (action.isOnlyIcon) {
                                        return (
                                            <div onClick={(_) => this.selectMenuAction(row, action.value)} className={style['icon-button']}>
                                                <SfanIcon name={action.icon} color="black" className={style['icon-button-icons']} />
                                            </div>
                                        );
                                    }
                                    return (
                                        <DropdownItem 
                                            className={classNames(style['menu-actions'], { [style['menu-actions-icons']]: action.icon })} 
                                            key={action + index} 
                                            onClick={(_) => this.selectMenuAction(row, action.value)}
                                        >
                                            {action.icon ? (
                                                <SfanIcon name={action.icon} color="white" className={style['action-icons']} />
                                            ) : ''}
                                            {action.label}
                                        </DropdownItem>
                                    );
                                })}
                            </div>
                        </td>
                    );

                }
                case 'link': {
                    if (row[key]) {
                        return (
                            <td
                                className={classNames(style['td-link'], { [style['td-right']]: this.props.headers[key].isRight })}
                                key={`cell-${index}-${key}`}
                                onClick={(_) => this.selectMenuAction(row, key)}
                            >
                                <span>
                                    {row[key] ? row[key] : ''}
                                </span>
                            </td>
                        );
                    }
                    return (
                        <td
                            key={`cell-${index}-${key}`}
                            onClick={(_) => this.handleOnClick(row)}
                        >
                            {row[key] ? row[key] : ''}
                        </td>
                    );
                }
                default: {
                    return (<td
                        className={classNames({ [style['td-right']]: this.props.headers[key].isRight })}
                        key={`cell-${index}-${key}`}
                        onClick={(_) => this.handleOnClick(row)}
                    >
                        {row[key] ? row[key] : ''}
                    </td>
                    );
                }
            }
        });
        return (
            <tr
                className={style['table-body-row']}
                key={`row-${index}`}
            >
                {tableRow}
            </tr>
        );
    }

    render() {
        return (
            <tbody>
                {this.props.data ? this.props.data.map((row, index) => this.renderField(row, index)) : (<tr></tr>)}
            </tbody>
        );
    }
}