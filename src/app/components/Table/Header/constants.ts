import { HeadersInterface } from '../constants';

export interface Props {
    headers: HeadersInterface;
    sort?: SortInterface;
    onSortChange?(action: any): void;
}

export interface State {
    sort: {
        field: string;
        type: 'asc' | 'desc';
    };
}

export interface SortInterface {
    field: string;
    type: 'asc' | 'desc';
}
