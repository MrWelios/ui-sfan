import * as React from 'react';
import * as classNames from 'classnames';
import { get } from 'lodash';

import * as style from './style.css';
import { Props, State } from './constants';
import { SfanIcon } from 'app/components/Icon';

export class SfanTableHeaders extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);

        this.state = {
            sort: {
                field: get(props, 'sort.field', ''),
                type: get(props, 'sort.type', ''),
            },
        };
    }

    handleOnClick = (isSort: boolean, key: string) => {
        if (isSort && this.props.onSortChange) {
            const typeChange = key === this.state.sort.field ? (this.state.sort.type === 'asc' ? 'desc' : 'asc') : 'asc';

            (this.refs[key] as HTMLElement).style.transform = typeChange === 'desc' ? 'rotate(0deg)' : 'rotate(180deg)';

            this.setState({
                sort: {
                    field: key,
                    type: typeChange,
                },
            });
            this.props.onSortChange({
                field: key,
                type: typeChange,
            });
        }
    }

    renderSort = (key, isSort) => {
        if (isSort) {
            return (
                <div ref={key} className={style['sort-block']}>
                    <SfanIcon
                        className={classNames(style['sort-icon'], { [style['head-cell-show']]: key === this.state.sort.field })}
                        name="arrow-drop-down"
                        color="black"
                    />
                </div>
            );
        }

        return '';
    }

    renderHeaderCells = () => Object.keys(this.props.headers).map((key) => (
        <th
            key={`headers-${this.props.headers[key].label}`}
            className={classNames(
                { [style['is-sorted']]: this.props.headers[key].isSort },
            )}
            onClick={(_) => this.handleOnClick(this.props.headers[key].isSort, key)}
        >
            <div className={classNames(
                style['head-cell'],
                { [style['head-cell-center']]: this.props.headers[key].isCenter },
                { [style['head-cell-right']]: this.props.headers[key].isRight },
            )}>
                {this.props.headers[key].label}
                {this.renderSort(key, this.props.headers[key].isSort)}
            </div>
        </th>
    ))

    render() {
        return (
            <thead>
                <tr>
                    {this.renderHeaderCells()}
                </tr>
            </thead>
        );
    }
}