export interface Props {
    headers: HeadersInterface;
    count?: number;
    limit?: number;
    offset?: number;
    sort?: {
        field: string;
        type: 'asc' | 'desc';
    };
    data: DataInterface[];
    onSortChange?(action: any): void;
    paginationClick?(action: any): void;
    onRowClick?(action: any): void;
    onMenuClick?(row: any, action: string): void;
}

export interface State {
}

export interface HeadersInterface {
    [index: string]: {
        label: string;
        isSort: boolean;
        isRight?: boolean;
        isCenter?: boolean;
        type?: string;
    };
}

export interface DataInterface {
    [index: string]: string | ActionInterface[];
}

export interface ActionInterface {
    value: string | number;
    label:  string;
}
