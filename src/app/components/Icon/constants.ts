export interface Props {
    name: string;
    className?: string;
    color: string;
    x?: string;
    y?: string;
    width?: string;
    height?: string;
}

export interface State {
}

export const COLORS = {
    white: '#ffffff',
    gray: '#c9cedf',
    blue: '#2da6c5',
};
