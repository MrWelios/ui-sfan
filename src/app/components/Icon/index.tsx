import * as React from 'react';

import 'assets/img/icons/icons.svg';
import { Props, State, COLORS } from './constants';

export class SfanIcon extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
    }

    render() {

        const { name, color, ...rest } = this.props;

        return (
            <svg {...rest}>
                <use xlinkHref={`#icons_${name}`} fill={COLORS[color]} />
            </svg>
        );

    }
}