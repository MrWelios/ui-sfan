import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { eventListReducer } from './eventsReducers';
import { 
    teamListReducer,
    competitionListReducer,
} from './appReducers';

export interface RootState {
    routing: any;
    eventListReducer: any;
    teamListReducer: any;
    competitionListReducer: any;
}

export const RootReducer = combineReducers<RootState>({
    routing: routerReducer,
    eventListReducer,
    teamListReducer,
    competitionListReducer,
});
