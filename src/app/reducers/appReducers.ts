import { 
    SET_TEAM_LIST,
    SET_COMPETITION_LIST,
} from 'app/constants/appConstants';

export function teamListReducer(state: any[] = [], action: ActionPayload) {
    switch (action.type) {
        case SET_TEAM_LIST: 
            return action.payload;
        default:
            return state;
    }
}

export function competitionListReducer(state: any[] = [], action: ActionPayload) {
    switch (action.type) {
        case SET_COMPETITION_LIST: 
            return action.payload;
        default:
            return state;
    }
}