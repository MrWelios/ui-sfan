import { SET_EVENT_LIST } from 'app/constants/eventConstants';

export function eventListReducer(state: any = {}, action: ActionPayload) {
    switch (action.type) {
        case SET_EVENT_LIST: 
            return action.payload;
        default:
            return state;
    }
}