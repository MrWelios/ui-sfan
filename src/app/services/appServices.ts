export function handleResponse(response) {
    if (response.status === 401 && typeof(window) !== 'undefined') {
        localStorage.removeItem('JWT');
        window.location.href = '/';
    }
    if (!response.ok) {
        return response.json().then(json => Promise.reject(json));
    }

    return response.json();
}

export function handleResponseWitchOutJson(response) {
    if (response.status === 401 && typeof(window) !== 'undefined') {
        localStorage.removeItem('JWT');
        window.location.href = '/';
    }
    if (!response.ok) {
        return response.json().then(json => Promise.reject(json));
    }

    return response;
}

export function getDefaultUrl() {
    if (typeof(window) !== 'undefined') {
        return window.DEFAULT_URL;
    }
    return process.env.DEFAULT_URL;
}

export function getHeader(customHeaders?) {
    const myHeaders = new Headers();

    myHeaders.append('Content-Type', 'application/json');
    if (typeof(window) !== 'undefined' && localStorage.getItem('JWT')) {
        myHeaders.append('Authorization', `Bearer ${localStorage.getItem('JWT')}`);
    }

    if (customHeaders) {
        Object.keys(customHeaders).forEach((key) => {
            myHeaders.append(key, customHeaders[key]);
        });
    }

    return myHeaders;
}

export function getApi() {
    return process.env.API;
}

export function getInitCookie() {
    if (typeof(window) !== 'undefined') {
        return window.INIT_COOKIE;
    }
    return process.env.INIT_COOKIE;
}

export function getUrl(params): string {
    let str = '';
    Object.keys(params).forEach((key) => {
        if (str !== '') {
            str += '&';
        }
        str += getUrlData(key, params[key]);
    });

    return str;
}

export function getUrlData(key, params): string {
    if (Array.isArray(params)) {
        return params.map(value => `${key}=${encodeURIComponent(value)}`).join('&');
    }
    return `${key}=${encodeURIComponent(params)}`;
}

export function getDate(date: Date, isHour?: boolean): string {
    if (date) {
        // tslint:disable-next-line:prefer-template
        return date.getFullYear() + '-' 
            + ( date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1) + '-' 
            + ( date.getDate() <= 9 ? '0' : '') + date.getDate() 
            + ( isHour ? ( ' ' + ( date.getHours() < 9 ? '0' : '') +  date.getHours() + ':' 
            + ( date.getMinutes() <= 9 ? '0' : '') + date.getMinutes() + ':'
            + ( date.getSeconds() <= 9 ? '0' : '') + date.getSeconds() ) : '');
    }
    return '';
}

export function getTeamList() {
    const requestOptions = {
        method: 'GET',
        headers: getHeader(),
    };

    return fetch(`${getApi()}/v1/teams/`, requestOptions)
        .then(handleResponse)
        .then((res) => res);
}

export function getCompetitionList() {
    const requestOptions = {
        method: 'GET',
        headers: getHeader(),
    };

    return fetch(`${getApi()}/v1/competitions/`, requestOptions)
        .then(handleResponse)
        .then((res) => res);
}
