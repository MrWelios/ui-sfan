import {
    getHeader,
    getUrl,
    getApi,
    handleResponse,
    getDate,
} from './appServices';

export function getEventsList(filter) {
    const requestOptions = {
        method: 'GET',
        headers: getHeader(),
    };

    const vars = {
        'offset': filter.offset ? filter.offset.toString() : '',
        'limit': filter.limit ? filter.limit.toString() : '',
        'team': filter.team.map(row => row.value),
        'competition': filter.competition.map(row => row.value),
        'order': `${filter.order.field}:${filter.order.type}`,
        'periodFrom': filter.dateFrom ? `${getDate(filter.dateFrom)}T00:00:00` : '',
        'periodTill': filter.dateTill ? `${getDate(filter.dateTill)}T23:59:59` : '',
    };

    const params = getUrl(vars);
    return fetch(`${getApi()}/v1/events/?${params}`, requestOptions)
        .then(handleResponse)
        .then((res) => res);
}

export function saveEventStreamingURL(eventID, url) {
    const requestOptions = {
        method: 'POST',
        headers: getHeader(),
        body: JSON.stringify({
            eventID,
            url,
        }),
    };

    return fetch(`${getApi()}/v1/event/streaming/`, requestOptions)
        .then(handleResponse)
        .then((res) => res);
}

export function saveEventScoreURL(eventID, scoreURL) {
    const requestOptions = {
        method: 'POST',
        headers: getHeader(),
        body: JSON.stringify({
            eventID,
            scoreURL,
        }),
    };

    return fetch(`${getApi()}/v1/event/score/`, requestOptions)
        .then(handleResponse)
        .then((res) => res);
}

export function saveEventReviews(eventReviews) {
    const requestOptions = {
        method: 'POST',
        headers: getHeader(),
        body: JSON.stringify({
            eventReviews,
        }),
    };

    return fetch(`${getApi()}/v1/events/reviews/`, requestOptions)
        .then(handleResponse)
        .then((res) => res);
}
