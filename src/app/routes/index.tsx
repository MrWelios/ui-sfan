import { EventList } from 'app/containers';

export const Routes = () => ([
    {
        path: '/',
        exact: true,
        component: EventList,
    },
]);
