import * as React from 'react';
import { connect } from 'react-redux';
import Helmet from "react-helmet";
import * as classNames from 'classnames';
// import * as i18next from 'i18next';

import * as style from './style.css';
import { RootState } from 'app/reducers';
import { AppNamespace } from './constants';
import renderRoutes from 'react-router-config/renderRoutes';
import { Routes } from 'app/routes';
import { SfanSidebar, SfanNavbar } from 'app/components';

class App extends React.Component<AppNamespace.Props, AppNamespace.State> {

    constructor(props: any) {
        super(props);

        // i18next.init({
        //     lng: selectedLang,
        //     resources: langs[selectedLang]
        // });
    }

    render() {
        return (
            <div>
                <Helmet
                    title=""
                />
                <SfanNavbar />
                <SfanSidebar />
                <div className={classNames('ml-sm-auto', style['app-main'])}>
                    {renderRoutes(Routes())}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: RootState) {
    const { } = state;

    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App)

export { connectedApp as App };
