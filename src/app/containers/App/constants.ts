import { RouteComponentProps } from 'react-router';

export namespace AppNamespace {
    export interface Props extends RouteComponentProps<void> {
        route: any;
        dispatch: any;
    }
  
    export interface State {
    }
}