import * as React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Button, Input, Row } from 'reactstrap';
import { cloneDeep } from 'lodash';

import { HEADER, FILTER, Props, State } from './constants';
import { RootState } from 'app/reducers';
import * as style from './style.css';
import { SfanTable, SfanModal, SfanFilter } from 'app/components';
import { getEventListAction } from 'app/actions/eventActions';
import { getTeamListAction, getCompetitionListAction } from 'app/actions/appActions';
import { saveEventStreamingURL, saveEventScoreURL, saveEventReviews } from 'app/services/eventServices';

class EventList extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        const initFilter = {
            competition: FILTER.competition.value,
            team: FILTER.team.value,
            offset: 0,
            limit: 25,
            order: {
                field: 'started_at',
                type: 'asc',
            },
            time: FILTER.time.value,
        };

        this.state = {
            ...initFilter,
            isOpenRenderModal: false,
            isOpenRenderScoreModal: false,
            isOpenRenderReviewModal: false,
            reviews: [],
            editRow: null,
            order: {
                field: 'started_at',
                type: 'asc',
            },
        };
        this.props.dispatch(getTeamListAction());
        this.props.dispatch(getCompetitionListAction());

        this.getList(initFilter);

    }

    getList = (formChanges?) => {
        const form = {
            limit: this.state.limit,
            offset: this.state.offset,
            order: this.state.order,
            competition: this.state.competition,
            team: this.state.team,
            dateFrom: this.state.time.startDate,
            dateTill: this.state.time.endDate,
        };
        if (formChanges) {
            Object.keys(formChanges).forEach((key) => {
                if (key === 'time') {
                    form.dateFrom = formChanges[key].startDate;
                    form.dateTill = formChanges[key].endDate;
                } else {
                    form[key] = formChanges[key];
                }
            });
        }
        const { dispatch } = this.props;
        dispatch(getEventListAction(form));
    }

    onSortChange = (order) => {
        this.setState({ order });
        this.getList({ order });
    }

    onRowClick = (event) => {
        console.log(event);
    }

    onMenuClick = (row, key) => {
        switch (key) {
            case 'edit': 
                this.setState({
                    isOpenRenderModal: true,
                    editRow: row,
                });
                break;
            case 'edit-scoreUrl': 
                this.setState({
                    isOpenRenderScoreModal: true,
                    editRow: row,
                });
                break;
            case 'edit-reviews': 
                this.setState({
                    isOpenRenderReviewModal: true,
                    editRow: row,
                    reviews: row.reviews.map(review => ({
                        ID: review.id,
                        eventID: review.event_id,
                        url: review.url,
                    })),
                });
                break;
            case 'scoreUrl': 
                window.open(row.scoreUrl);
                break;
            case 'play': 
                window.open(row.url);
                break;
            default: 
        }
    }

    paginationClick = (offset) => {
        this.setState({ offset });
        this.getList({ offset });
    }

    closeModal = () => {
        this.setState({
            isOpenRenderScoreModal: false,
            isOpenRenderReviewModal: false,
            isOpenRenderModal: false,
            editRow: null,
            reviews: [],
        });
    }

    saveUrl = () => {
        saveEventStreamingURL(this.state.editRow.id, this.state.editRow.url).then(
            (_res) => {
                this.getList();
                this.closeModal();
            },
            (error) => {
                console.log('error');
                console.log(error);
            },
        );
    }

    saveReview = () => {
        saveEventReviews(this.state.reviews).then(
            (_res) => {
                this.getList();
                this.closeModal();
            },
            (error) => {
                console.log('error');
                console.log(error);
            },
        );
    }

    saveScoreUrl = () => {
        saveEventScoreURL(this.state.editRow.id, this.state.editRow.scoreUrl).then(
            (_res) => {
                this.getList();
                this.closeModal();
            },
            (error) => {
                console.log('error');
                console.log(error);
            },
        );
    }

    submitFilter = (e) => {
        this.setState({...e});
        this.getList(e);
    }

    onChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState((prew) => ({
            editRow: {
                ...prew.editRow,
                [name]: value,
            },
        }));
    }

    renderEditModal = () => {
        if (this.state.isOpenRenderModal) {
            return (
                <SfanModal onClose={this.closeModal}>
                    Edit streaming url
                    <Input 
                        name="url"
                        onChange={this.onChange}
                        value={this.state.editRow.url}
                    />

                    <Button className={style['save-button']} onClick={this.saveUrl}>Save</Button>
                </SfanModal>
            );
        }
    }

    onChangeReview = (event, index) => {
        const target = event.target;
        const value = target.value;
        const reviews = cloneDeep(this.state.reviews);
        reviews[index].url = value;
        this.setState({
            reviews,
        });
    }

    addReview = () => {
        this.setState((prewState) => ({
            reviews: [
                ...prewState.reviews,
                {
                    url: '',
                    eventID: this.state.editRow.id,
                },
            ],
        }));
    }

    renderEditReviewsModal = () => {
        if (this.state.isOpenRenderReviewModal) {
            return (
                <SfanModal onClose={this.closeModal}>
                    Edit reviews url
                    {this.state.reviews.map((review, index) => (
                        <Input 
                            key={`review-${index}`}
                            name="review"
                            className={style['review-input']}
                            onChange={(e) => this.onChangeReview(e, index)}
                            value={review.url}
                        />
                    ))}

                    <Row>
                        <Button className={style['save-button']} onClick={this.saveReview}>Save</Button>
                        <Button className={style['save-button']} onClick={this.addReview}>Add</Button>
                    </Row>
                </SfanModal>
            );
        }
    }

    renderEditScoreModal = () => {
        if (this.state.isOpenRenderScoreModal) {
            return (
                <SfanModal onClose={this.closeModal}>
                    Edit Score url
                    <Input 
                        name="scoreUrl"
                        onChange={this.onChange}
                        value={this.state.editRow.scoreUrl}
                    />

                    <Button className={style['save-button']} onClick={this.saveScoreUrl}>Save</Button>
                </SfanModal>
            );
        }
    }

    render() {
        const dataFilter = cloneDeep(FILTER);

        dataFilter.team.selectList = [
            {
                value: 0,
                label: 'All',
            },
            ...this.props.teamList,
        ];
        dataFilter.competition.selectList = [
            {
                value: 0,
                label: 'All',
            },
            ...this.props.competitionList,
        ];

        return (
            <div className={style.content}>
                <Helmet
                    title=""
                />

                {this.renderEditModal()}
                {this.renderEditScoreModal()}
                {this.renderEditReviewsModal()}

                <SfanFilter filterData={dataFilter} onSubmit={this.submitFilter} />

                <SfanTable
                    headers={HEADER}
                    data={this.props.list.data || []}
                    sort={this.state.order}
                    limit={this.state.limit}
                    offset={this.state.offset}
                    count={this.props.list.total}
                    onSortChange={this.onSortChange}
                    onRowClick={this.onRowClick}
                    onMenuClick={this.onMenuClick}
                    paginationClick={this.paginationClick}
                />
            </div>
        );
    }
}

function mapStateToProps(state: RootState) {
    const {
        teamListReducer,
        competitionListReducer,
        eventListReducer,
    } = state;
    return {
        teamList: teamListReducer,
        competitionList: competitionListReducer,
        list: eventListReducer,
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        dispatch,
    };
}

const connected = connect(mapStateToProps, mapDispatchToProps)(EventList);

export { connected as EventList };