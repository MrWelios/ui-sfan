export interface Props {
    dispatch: any;
    teamList: any[];
    competitionList: any[];
    list: any;
}

export interface State {
    competition: any;
    team: any;
    time: any;
    order: {
        field: string;
        type: 'asc' | 'desc';
    };
    limit: number;
    offset: number;
    isOpenRenderModal: boolean;
    isOpenRenderScoreModal: boolean;
    isOpenRenderReviewModal: boolean;
    reviews: any[];
    editRow: any;
}


export const HEADER = {
    startedAt: {
        label: 'Time',
        isSort: true,
    },
    team1Name: {
        label: 'Team 1',
        isSort: true,
    },
    team2Name: {
        label: 'Team 2',
        isSort: true,
    },
    competition: {
        label: 'Competition',
        isSort: true,
    },
    status: {
        label: 'Status',
        isSort: false,
    },
    score: {
        label: 'Score',
        isSort: false,
        isCenter: true,
        type: 'actions',
    },
    reviewsLink: {
        label: 'Reviews',
        isSort: false,
        type: 'actions',
    },
    liveStreaming: {
        label: 'Live streaming',
        isSort: false,
        type: 'actions',
    },
};

export const FILTER = {
    time: {
        label: 'Time',
        type: 'time-interval',
        value: {
            startDate: new Date(),
            endDate: new Date(),
        },
    },
    competition: {
        label: 'Competition',
        type: 'multi-select',
        selectList: [],
        value: [],
    },
    team: {
        label: 'Team',
        type: 'multi-select',
        selectList: [],
        value: [],
    },

};
