import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

import { history } from '../../.';
import { loggerMiddleware, eventMiddleware, appMiddleware } from 'app/middleware';
import { RootReducer, RootState } from 'app/reducers';


export function configureStore(initialState?: RootState) {
    let middleware = applyMiddleware(
        loggerMiddleware, 
        eventMiddleware,
        appMiddleware, 
        thunkMiddleware, 
        routerMiddleware(history),
    );

    if (process.env.NODE_ENV === 'development') {
        middleware = composeWithDevTools(middleware);
    }

    const store = createStore(RootReducer, initialState, middleware) as Store<RootState>;

    return store;
}
