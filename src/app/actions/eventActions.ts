import { createAction } from 'redux-actions';

import { SET_EVENT_LIST, GET_EVENT_LIST } from 'app/constants/eventConstants';
import { getEventsList } from 'app/services/eventServices';

export const getEventListAction = (filter) => {
    const request = createAction(GET_EVENT_LIST, arg => arg);
    const success = createAction(SET_EVENT_LIST, arg => arg);
    return dispatch => {
        dispatch(request({}));
        return getEventsList(filter)
            .then(
                (data) => {
                    dispatch(success(data));
                },
                (error) => {
                    console.log('error');
                    console.log(error);
                },
            );
    };
};
