import { createAction } from 'redux-actions';

import { SET_TEAM_LIST, GET_TEAM_LIST, GET_COMPETITION_LIST, SET_COMPETITION_LIST } from 'app/constants/appConstants';
import { getTeamList, getCompetitionList } from 'app/services/appServices';

export const getTeamListAction = () => {
    const request = createAction(GET_TEAM_LIST, arg => arg);
    const success = createAction(SET_TEAM_LIST, arg => arg);
    return dispatch => {
        dispatch(request({}));
        return getTeamList()
            .then(
                (data) => {
                    dispatch(success(data));
                },
                (error) => {
                    console.log('error');
                    console.log(error);
                },
            );
    };
};

export const getCompetitionListAction = () => {
    const request = createAction(GET_COMPETITION_LIST, arg => arg);
    const success = createAction(SET_COMPETITION_LIST, arg => arg);
    return dispatch => {
        dispatch(request({}));
        return getCompetitionList()
            .then(
                (data) => {
                    dispatch(success(data));
                },
                (error) => {
                    console.log('error');
                    console.log(error);
                },
            );
    };
};
