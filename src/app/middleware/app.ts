import { SET_TEAM_LIST, SET_COMPETITION_LIST } from 'app/constants/appConstants';

export const appMiddleware = (_store) => next => (action: ActionPayload) => {
    switch (action.type) {
        case SET_TEAM_LIST:
            action.payload = action.payload.map((row) => ({
                label: row.name,
                value: row.id,
            }));
            break;
        case SET_COMPETITION_LIST:
            action.payload = action.payload.map((row) => ({
                label: row.name,
                value: row.id,
            }));
            break;
        default:
    }
    return next(action);
};
