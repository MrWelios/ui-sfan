import { SET_EVENT_LIST } from 'app/constants/eventConstants';
import { getDate } from 'app/services/appServices';

export const eventMiddleware = (_store) => next => (action: ActionPayload) => {
    switch (action.type) {
        case SET_EVENT_LIST:
            action.payload.data = action.payload.data.map((row) => ({
                ...row,
                startedAt: getDate(new Date(row.startedAt), true),
                status: getStatus(new Date(row.startedAt)),
                reviews: JSON.parse(row.reviews).filter(review => review !== null),
                reviewsLink: [
                    
                    ...(
                        JSON.parse(row.reviews).filter(review => review !== null).length > 0 ?
                        [{
                            value: 'review-play',
                            label: '',
                            icon: 'description',
                            isOnlyIcon: true,
                        }]
                        :
                        [{
                            value: '',
                            label: '',
                            icon: '',
                            isOnlyIcon: true,
                        }]
                    ),
                    {
                        value: 'edit-reviews',
                        label: '',
                        icon: 'edit',
                    },
                ],
                liveStreaming: [
                    ...(row.url ? [{
                        value: 'play',
                        label: '',
                        icon: 'play',
                        isOnlyIcon: true,
                    }] : [{
                        value: '',
                        label: '',
                        icon: '',
                        isOnlyIcon: true,
                    }]),
                    {
                        value: 'edit',
                        label: '',
                        icon: 'edit',
                    },
                ],
                score: [
                    ...(row.scoreUrl ? [{
                        value: 'scoreUrl',
                        label: '',
                        icon: 'ball',
                        isOnlyIcon: true,
                    }] : [{
                        value: '',
                        label: '',
                        icon: '',
                        isOnlyIcon: true,
                    }]),
                    {
                        value: 'edit-scoreUrl',
                        label: '',
                        icon: 'edit',
                    },
                ],
            }));
            break;
        default:
    }
    return next(action);
};


const getStatus = (date: Date): 'coming soon' | 'live' | 'finished' => {
    if (date.getTime() > new Date().getTime() + 110 * 60 * 1000) {
        return 'coming soon';
    }

    if (date.getTime() > new Date().getTime() && date.getTime() < new Date().getTime() + 110 * 60 * 1000) {
        return 'live';
    }

    return 'finished';
};
