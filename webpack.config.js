const Webpack = require('webpack');
var fs = require('fs');
const Path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

const isProduction = process.argv.indexOf('-p') >= 0;
const outPath = Path.join(__dirname, './dist');
const sourcePath = Path.join(__dirname, './src');
var crypto = require('crypto');
const hash = crypto.createHash('md5').update(((new Date()).getTime()).toString()).digest("hex");
let config = process.env.npm_config_config ? process.env.npm_config_config : 'prod';

const confData = require(`./config/${config}.json`);

console.log('confData');
console.log(confData);

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });


function srcPath(subdir) {
    return Path.join(__dirname, "src", subdir);
}

module.exports = function (env) {
    const base = {
        mode: isProduction ? 'production' : 'development',
        context: sourcePath,
        entry: './server.tsx',
        output: {
            path: outPath,
            publicPath: '/',
            filename: 'server.js',

        },
        resolve: {
            alias: {
                app: srcPath('app'),
                assets: srcPath('assets'),
            },
            extensions: ['.js', '.ts', '.tsx'],
            mainFields: ['browser', 'main']
        },
        target: 'web',
        module: {
            rules: [
                // .ts, .tsx
                {
                    test: /\.tsx?$/,
                    use: isProduction
                        ? 'awesome-typescript-loader'
                        : [
                            'babel-loader',
                            'awesome-typescript-loader'
                        ]
                },
                // css
                {
                    test: /\.s?css$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                sourceMap: !isProduction,
                                minimize: true,
                                importLoaders: 1,
                                localIdentName: '[name]__[local]___[hash:base64:5]'
                            }
                        },
                    ]
                },
                // svg
                {
                    test: /\.svg$/,
                    use: [
                        { loader: 'svg-sprite-loader' },
                    ]
                },
            ],
        },
        plugins: [
            new Webpack.DefinePlugin({
                'process.env.API': JSON.stringify(confData.api),
            }),
            // new Webpack.optimize.AggressiveMergingPlugin(),
            // new ExtractTextPlugin({
            //     filename: 'styles.css',
            //     disable: !isProduction,
            //     allChunks: true
            // }),
            new HtmlWebpackPlugin({
                template: 'index.html',
                // favicon: 'assets/icons/favicon.ico'
            }),
            new CopyWebpackPlugin([
                { from: 'assets', to: 'assets' },
                // { from: '../config', to: 'config' },
                // { from: 'assets/img/icons/icons-short.svg', to:  'assets/img/icons/icons-' + svgShort + 'short.svg' },
                // { from: 'assets/img/icons/icons-full.svg', to:  'assets/img/icons/icons-' + svgFull + 'full.svg' }

            ]),
        ],
        devServer: {
            contentBase: sourcePath,
            hot: true,
            stats: {
                warnings: false
            },
        },
        node: {
            console: false,
            global: false,
            process: false,
            Buffer: false,
            __filename: false,
            __dirname: false
        }
    };

    base.entry = './index.tsx';
    base.output.filename = 'client-' + hash + '.js';
    return base;
}
