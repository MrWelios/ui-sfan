/** Global definitions for development **/

// for style loader
declare module '*.css' {
  const styles: any;
  export = styles;
}

declare module '*.svg' {
  const styles: any;
  export = styles;
}
interface Window { __PRELOADED_STATE__: any; INIT_COOKIE: string; API: string, DEFAULT_URL: string, REF_URL: string,
  SENTRY_DSN: string, RECAPTCHA_KEY: string, IP: string }
interface Global { API: string}
declare const API;
